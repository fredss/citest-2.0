import java.util.ArrayList;
import java.util.List;

public class ClassToTest {
    private List<Integer> list = new ArrayList<>();

    public int get(int index){
        return list.get(index);
    }

    public void add(int integer){
        list.add(integer);
    }

    public long product(){
        long result = 1;
        for(Integer i : list){
            result = result * i;
        }
        return result;
    }
}
