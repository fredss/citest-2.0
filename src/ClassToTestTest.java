import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;

public class ClassToTestTest {

    @Test
    public void get() {
        ClassToTest test = new ClassToTest();
        test.add(5);
        assertEquals(5,test.get(0));
    }

    @Test
    public void product() {
        ClassToTest test = new ClassToTest();
        test.add(5);
        test.add(7);
        test.add(9);
        assertEquals(315, test.product());
    }
}